module.exports = {
  "plugins": [
  	"node",
  	"prettier"
  ],
  "extends": [
  	"eslint:recommended", 
  	"plugin:node/recommended",
  	"prettier"
  ],
  "rules": {
      "node/exports-style": ["error", "module.exports"],
      "prettier/prettier": "error"
  }
};