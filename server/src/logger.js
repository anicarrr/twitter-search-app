import logger from "winston";

logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, { level: "debug", colorize: true });

export default logger;
