import http from "http";
import express from "express";
import dotenv from "dotenv";
import logger from "./logger";
import service from "./service";

// Load env vars from .env
dotenv.config();

const PORT = 8080;

const app = express();
app.server = http.createServer(app);

app.get("/api/search/:word", async (req, res) => {
  const word = req.params.word;
  logger.verbose("GET " + req.url);

  const info = await service.getInfo(word);

  if (info.notFound) {
    res.sendStatus(404);
  }

  res.send(info);
});

app.get("/api/more/:word/:lastTweetId", async (req, res) => {
  const word = req.params.word;
  const lastTweetId = req.params.lastTweetId;
  logger.verbose("GET " + req.url);

  const info = await service.getMore(word, lastTweetId);
  if (info.notFound) {
    res.sendStatus(404);
  }

  res.send(info);
});

app.get("/api/search/:word/:id", async (req, res) => {
  const word = req.params.word;
  const id = req.params.id;
  logger.verbose("GET " + req.url);

  const info = await service.getById(word, id);

  if (info.notFound) {
    res.sendStatus(404);
  }

  res.send(info);
});

app.server.listen(PORT);
console.log(`Server started on port ${app.server.address().port}`);
