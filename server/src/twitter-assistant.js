import Twit from "twit";
import logger from "winston";
import { ObjectId } from "mongodb";

/**
 * Fetch 100 tweets, maybe less (limited by twitter api)
 * @param {Object} options 
 * @returns {Promise<Array>} 
 */
async function getTweets(options) {
  const twitter = new Twit({
    consumer_key: process.env.TWITTER_CONSUMER_KEY,
    consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
    app_only_auth: true
  });

  const opts = {
    count: 100,
    since_id: 1
  };

  Object.assign(opts, options);

  const response = await twitter.get("search/tweets", opts);

  if (response.data.errors) {
    logger.verbose(response.data.errors);
    return;
  }

  const word = options.q;
  const tweets = response.data.statuses.filter(t => t.id_str !== opts.max_id);

  if (!tweets.length) {
    var noMore = options.lastTweetId ? "more" : "";
    logger.verbose(`Search term: '${word}' - no ${noMore}tweets found`);
    tweets.notFound = true;
    return tweets;
  }

  logger.verbose(
    `Search term: '${word}' - ${tweets.length} new tweets from Twitter API`
  );

  return tweets.map(t => ({
    id: t.id_str,
    text: t.text,
    user: t.user.screen_name,
    avatar: t.user.profile_image_url
  }));
}

/**
 * Get tweets information given a word
 * @param {String} word 
 * @returns {Promise<Object>}
 */
async function getInfo(options) {
  const tweets = await getTweets(options);

  if (tweets.notFound) {
    return tweets;
  }

  const word = options.q;
  const tweetIds = tweets.map(x => x.id);
  const lastTweetId = tweetIds.sort((a, b) => a - b)[0];

  return {
    word: word,
    lastTweetId: lastTweetId,
    tweets: tweets
  };
}

export default {
  getInfo: getInfo,
  getTweets: getTweets
};
