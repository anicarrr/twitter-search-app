import { MongoClient } from "mongodb";
import logger from "winston";
import DbMock from "./database-mock";

let db = null;
async function getDb() {
  if (db) {
    return db;
  }

  //In case mongoDB is not installed it will use a mocked database
  const url = process.env.MONGO_URL;
  if (!url) {
    logger.warn("Database URL not found, using db mock");
    db = new DbMock();
  } else {
    logger.info(`Connecting to db at ${url}...`);
    db = await MongoClient.connect(url);
    logger.info("Connected to db");
  }

  return db;
}

async function getInfoCollection() {
  const INFO_COL = "info";
  const db = await getDb();
  return db.collection(INFO_COL);
}

async function getInfo(word) {
  const coll = await getInfoCollection();
  const info = await coll.findOne(
    { word },
    { _id: 0, tweets: { $slice: 100 } }
  );
  return info;
}

async function getById(word, tweetId) {
  const coll = await getInfoCollection();
  const info = await coll
    .aggregate([
      {
        $match: {
          word: word
        }
      },
      {
        $unwind: {
          path: "$tweets"
        }
      },
      {
        $match: {
          "tweets.id": tweetId
        }
      },
      {
        $project: {
          id: "$tweets.id",
          text: "$tweets.text",
          avatar: "$tweets.avatar",
          user: "$tweets.user",
          _id: 0
        }
      }
    ])
    .next();

  return info;
}

async function getByRange(word, lastTweetId) {
  const coll = await getInfoCollection();
  const index = await coll
    .aggregate([
      {
        $match: {
          word: word
        }
      },
      {
        $project: {
          position: {
            $indexOfArray: ["$tweets.id", lastTweetId]
          }
        }
      }
    ])
    .next();

  var positionInArray = index ? index.position + 1 : 0;

  const info = await coll
    .aggregate([
      {
        $match: {
          word: word
        }
      },
      {
        $project: {
          tweets: { $slice: ["$tweets", positionInArray, 100] }
        }
      }
    ])
    .next();

  return info;
}

async function updateInfo(info) {
  const coll = await getInfoCollection();
  const word = info.word;
  const tweets = info.tweets;
  return coll.update({ word }, { $set: info }, { upsert: true });
}

async function updateList(info) {
  const coll = await getInfoCollection();
  const word = info.word;
  const tweets = info.tweets;
  return coll.update({ word }, { $push: { tweets: { $each: tweets } } });
}

export default {
  get: getInfo,
  getById: getById,
  getByRange: getByRange,
  update: updateList,
  save: updateInfo
};
