import logger from "winston";
import twitter from "./twitter-assistant.js";
import db from "./database-assistant";
import request from "request";
import moment from "moment";

function calculateCacheTimeExpiration(lastUpdate, CACHE_MINUTES) {
  var now = moment();
  var minutesDiff = now.diff(lastUpdate, "minutes");
  var hasExpire = minutesDiff >= CACHE_MINUTES;

  return hasExpire;
}

/**
 * Get all the information related with the given word 
 * @param {String} word 
 * @returns Promise<Object>} 
 */
async function getInfo(word) {
  const CACHE = process.env.CACHE === "true" ? true : false;
  const CACHE_MINUTES = process.env.CACHE_MINUTES;
  var cacheTimeout = true;

  logger.verbose(`Search term: ${word} - Getting info`);

  const storedInfo = await db.get(word);

  if (storedInfo) {
    cacheTimeout = calculateCacheTimeExpiration(
      storedInfo.lastUpdate,
      CACHE_MINUTES
    );
  }

  if (storedInfo && CACHE && !cacheTimeout) {
    logger.verbose(`Search term: '${word}' - Info recovered by cache`);
    return storedInfo;
  }

  if (!CACHE || cacheTimeout) {
    const options = { q: word };
    const newInfo = await twitter.getInfo(options);

    if (newInfo.notFound) {
      return newInfo;
    }

    newInfo.lastUpdate = new Date();

    db.save(newInfo);

    return newInfo;
  }
}

/**
 * Get an specific tweet
 * @param {String} word 
 * @param {String} tweetId 
 * @returns Promise<Object>} 
 */
async function getById(word, tweetId) {
  var storedTweet = await db.getById(word, tweetId);

  if (!storedTweet) {
    storedTweet = { notFound: true };
  }

  return storedTweet;
}

/**
 * Get tweets greater than the given lastTweetId
 * @param {String} word 
 * @param {String} lastTweetId 
 * @returns Promise<Object>} 
 */
async function getMore(word, lastTweetId) {
  const storedInfo = await db.getByRange(word, lastTweetId);

  if (storedInfo && storedInfo.tweets.length) {
    logger.verbose(
      `Search term: '${word}' - Info recovered by cache | ${storedInfo.tweets
        .length} more tweets`
    );
    storedInfo.word = word;
    return storedInfo;
  }

  //if tweets are not store we go for new ones greater than lastTweetId

  const options = {
    q: word,
    max_id: lastTweetId
  };

  const newTweets = await twitter.getTweets(options);

  if (newTweets.notFound) {
    return newTweets;
  }

  const newInfo = {
    word: word,
    lastUpdate: new Date(),
    tweets: newTweets
  };

  db.update(newInfo);

  return newInfo;
}

export default {
  getInfo: getInfo,
  getById: getById,
  getMore: getMore
};
