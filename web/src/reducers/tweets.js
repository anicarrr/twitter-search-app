export default (state = [], action) => {
  switch (action.type) {
    case "CLEAN_TWEET_LIST":
      return [];

    case "ADD_TWEETS_TO_LIST":
      return [...state, ...action.tweets];

    default:
      return state;
  }
};
