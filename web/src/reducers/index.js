import { combineReducers } from "redux";
import tweets from "./tweets";
import tweetsBuffer from "./tweetsBuffer";
import textbox from "./textbox";
import { routerReducer } from "react-router-redux";

const rootReducer = combineReducers({
  tweets,
  tweetsBuffer,
  textbox,
  router: routerReducer
});

export default rootReducer;
