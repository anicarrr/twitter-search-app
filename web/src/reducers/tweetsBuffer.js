export default (state = [], action) => {
  switch (action.type) {
    case "ADD_TWEETS_TO_BUFFER":
      return state.concat(action.tweets);

    case "DECREMENT_TWEETS_BUFFER":
      return state.slice(1);

    default:
      return state;
  }
};
