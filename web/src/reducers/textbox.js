const initialState = "";

export default (state = initialState, action) => {
  switch (action.type) {
    case "UPDATE_TEXTBOX_VALUE":
      return action.value;

    default:
      return state;
  }
};
