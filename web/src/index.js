import React from "react";
import ReactDOM from "react-dom";
import { Route, Switch } from "react-router-dom";
import {
  ConnectedRouter as Router,
  routerMiddleware
} from "react-router-redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import { createStore, applyMiddleware } from "redux";
import createHistory from "history/createBrowserHistory";
import { Provider } from "react-redux";

import reducer from "./reducers";
import App from "./App";
import TweetSelected from "./components/TweetSelected/TweetSelected";
import NoMatch from "./components/NoMatch/NoMatch";

import "./index.css";

const history = createHistory();
const middleware = routerMiddleware(history);

const store = createStore(
  reducer,
  composeWithDevTools(applyMiddleware(middleware, thunk))
);

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <Switch>
        <Route exact path="/" component={App} />
        <Route exact path="/:word" component={App} />
        <Route exact path="/:word/:id" component={TweetSelected} />
        <Route component={NoMatch} />
      </Switch>
    </Router>
  </Provider>,
  document.getElementById("root")
);
