import React, { Component } from "react";
import Tweet from "../Tweet/Tweet";
import PropTypes from "prop-types";

class TweetList extends Component {
  onTweetSelected = tweet => {
    this.props.tweetSelected(tweet);
  };

  render() {
    const { tweets } = this.props;
    if (!tweets || !tweets.length) return null;
    return (
      <div className="tweet-list">
        {tweets.map((tweet, i) => (
          <Tweet key={i} tweet={tweet} tweetSelected={this.onTweetSelected} />
        ))}
        <div style={{ height: "300px" }} />
      </div>
    );
  }
}

TweetList.propTypes = {
  tweetSelected: PropTypes.func.isRequired,
  tweets: PropTypes.array.isRequired
};

export default TweetList;
