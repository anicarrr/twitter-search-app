import React, { Component } from "react";
import Tweet from "../Tweet/Tweet";
import service from "../../utils/tweets-service";
import HeightMotion from "../HeightMotion/HeightMotion";
import Loader from "halogen/RingLoader";
import TweetNotFound from "../../assets/tweet_not_found.png";
import "./TweetSelected.css";

const DELAY = 1500;

class TweetSelected extends Component {
  state = {
    tweet: null,
    error: false,
    loading: false
  };

  componentDidMount = () => {
    if (this.props.location.state) return null;
    const word = this.props.match.params.word;
    const id = this.props.match.params.id;

    if (word && id) {
      this.getTweetById(word, id);
    }
  };

  componentWillMount() {
    const tweet = this.props.location.state
      ? this.props.location.state.tweet
      : null;

    this.setState({ tweet: tweet });
  }

  getTweetById = (word, tweetId) => {
    this.setState({ loading: true });

    setTimeout(() => {
      service
        .getById(word, tweetId)
        .then(this.load)
        .catch(this.loadError);
    }, DELAY);
  };

  load = info => {
    this.setState({
      tweet: info,
      loading: false
    });
  };

  loadError = info => {
    this.setState({
      error: true,
      loading: false,
      tweet: null
    });
  };

  tweetSelected = () => {};

  render() {
    const state = this.state;
    return (
      <div className="tweet-selected">
        {state.tweet ? (
          <Tweet
            tweet={state.tweet}
            tweetSelected={this.tweetSelected}
            single={true}
          />
        ) : (
          <div>
            {state.loading ? (
              <Loader color="#0aac8e" size="80px" />
            ) : (
              <img src={TweetNotFound} alt="Tweet not found" />
            )}
          </div>
        )}
        <HeightMotion show={state.error} height={40} className="landing">
          Sorry, something went wrong
        </HeightMotion>
      </div>
    );
  }
}

export default TweetSelected;
