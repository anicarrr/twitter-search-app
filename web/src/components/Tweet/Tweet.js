import React, { Component } from "react";
import PropTypes from "prop-types";
import "./Tweet.css";

class Tweet extends Component {
  state = {
    mounting: true
  };

  componentDidMount() {
    setTimeout(() => this.setState({ mounting: false }), 50);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.state.mounting;
  }

  onTweetSelected = () => {
    this.props.tweetSelected(this.props.tweet);
  };

  render() {
    const props = this.props;
    var className = this.state.mounting
      ? "tweet-container entrance"
      : "tweet-container";

    if (props.single) {
      className += " tweet-single";
    }

    return (
      <div className={className} onClick={this.onTweetSelected}>
        <div className="tweet-main">
          <img className="tweet-avatar" src={props.tweet.avatar} alt="avatar" />
          <div className="tweet">
            <div className="tweet-header">
              <strong className="tweet-fullname">@{props.tweet.user}</strong>
            </div>
            <span className="tweet-text">{props.tweet.text}</span>
          </div>
        </div>
      </div>
    );
  }
}

Tweet.propTypes = {
  tweet: PropTypes.object.isRequired,
  single: PropTypes.bool
};

export default Tweet;
