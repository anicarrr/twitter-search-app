import React from "react";
import TweetNotFound from "../../assets/tweet_not_found.png";
import "./NoMatch.css";

const NoMatch = () => {
  return (
    <div className="center-screen">
      <img src={TweetNotFound} alt="Page not found" />
    </div>
  );
};

export default NoMatch;
