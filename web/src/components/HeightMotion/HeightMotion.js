import React, { Component } from "react";
import { Motion, spring } from "react-motion";
import PropTypes from "prop-types";

class HeightMotion extends Component {
  get motionStyle() {
    const props = this.props;
    return props.show
      ? {
          height: spring(props.height),
          opacity: spring(1)
        }
      : {
          height: spring(0),
          opacity: spring(0)
        };
  }

  getStyle = snapshot => {
    return {
      height: `${snapshot.height}px`,
      opacity: snapshot.opacity
    };
  };

  render() {
    const props = this.props;
    return (
      <Motion style={this.motionStyle}>
        {snapshot => (
          <div style={this.getStyle(snapshot)} className={props.className}>
            {props.children}
          </div>
        )}
      </Motion>
    );
  }
}

HeightMotion.propTypes = {
  className: PropTypes.string,
  height: PropTypes.number.isRequired,
  show: PropTypes.bool.isRequired
};

export default HeightMotion;
