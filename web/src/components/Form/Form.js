import React, { Component } from "react";
import Spinner from "../Spinner/Spinner";
import TextboxButton from "../TextboxButton/TextboxButton";
import PropTypes from "prop-types";
import "./Form.css";

class Form extends Component {
  get inputStyle() {
    return this.props.loading ? { display: "none" } : {};
  }

  get spinnerStyle() {
    return this.props.loading
      ? { opacity: this.props.alpha }
      : { display: "none" };
  }

  get formStyle() {
    return {
      height: `${this.props.height}px`,
      width: `${this.props.width}px`,
      borderRadius: `${this.props.radius}px`,
      backgroundColor: `rgba(10,172,142,${this.props.alpha})`
    };
  }

  render() {
    const props = this.props;
    return (
      <div className="form" style={this.formStyle}>
        <Spinner style={this.spinnerStyle} />
        <TextboxButton
          onSubmit={props.onChange}
          placeholder="any word"
          style={this.inputStyle}
        />
      </div>
    );
  }
}

Form.propTypes = {
  loading: PropTypes.bool.isRequired,
  alpha: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  radius: PropTypes.number.isRequired,
  width: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired
};

export default Form;
