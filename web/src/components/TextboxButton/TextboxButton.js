import React, { Component } from "react";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../../actions";

class TextboxButton extends Component {
  handleSumbit = e => {
    if (!this.props.value) return;
    this.props.onSubmit(this.props.value);
  };

  handleChange = e => {
    this.props.actions.updateTextboxValue(e.target.value);
  };

  handleKeyPress = e => {
    if (e.key === "Enter") {
      this.handleSumbit(e);
    }
  };

  render() {
    const props = this.props;
    return (
      <div style={props.style} className="wrapper">
        <input
          type="text"
          value={props.value}
          onChange={this.handleChange}
          onKeyPress={this.handleKeyPress}
          placeholder={props.placeholder}
        />
        <button onClick={this.handleSumbit}>Load</button>
      </div>
    );
  }
}

TextboxButton.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  style: PropTypes.object.isRequired,
  placeholder: PropTypes.string.isRequired
};

const mapStateToProps = state => ({
  value: state.textbox
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
});

export { TextboxButton }; //for testing propuses
export default connect(mapStateToProps, mapDispatchToProps)(TextboxButton);
