import { connect } from "react-redux";
import { TextboxButton } from "./TextboxButton";

export class TextboxButtonMock extends TextboxButton {}

const mapStateToProps = state => ({
  value: state.textbox
});

export default connect(mapStateToProps)(TextboxButtonMock);
