import React from "react";
import { shallow } from "enzyme";
import { TextboxButtonMock } from "./TextboxButtonMock";

const setup = propOverrides => {
  const props = Object.assign(
    {
      onSubmit: jest.fn(),
      style: {},
      placeholder: "any word",
      value: ""
    },
    propOverrides
  );

  const component = shallow(<TextboxButtonMock {...props} />);

  return {
    props,
    component
  };
};

describe("component", () => {
  describe("TextboxButton", () => {
    it("initial render", () => {
      const { props, component } = setup();

      expect(component.find("div").hasClass("wrapper")).toBe(true);

      const input = component.find("input").props();
      expect(input.type).toBe("text");
      expect(input.value).toBe("");
      expect(input.placeholder).toEqual("any word");
      expect(input.onKeyPress).toBe(component.instance().handleKeyPress);
      expect(input.onChange).toBe(component.instance().handleChange);

      const button = component.find("button");
      expect(button.prop("onClick")).toBe(component.instance().handleSumbit);
      expect(button.text()).toBe("Load");
    });

    it("should not submit when inputtext value is empty", () => {
      const { props, component } = setup();

      const button = component.find("button");
      button.simulate("click");
      expect(props.onSubmit).not.toHaveBeenCalled();
    });
  });
});
