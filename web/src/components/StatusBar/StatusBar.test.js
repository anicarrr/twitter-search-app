import React from "react";
import { createRenderer } from "react-test-renderer/shallow";
import StatusBar from "./StatusBar";

const setup = propOverrides => {
  const props = Object.assign({}, propOverrides);

  const renderer = createRenderer();
  renderer.render(<StatusBar {...props} />);
  const output = renderer.getRenderOutput();

  return {
    props: props,
    output: output
  };
};

describe("component", () => {
  describe("StatusBar", () => {
    it("should render propertly", () => {
      const { output } = setup();
      expect(output.type).toBe("div");
      expect(output.props.className).toBe("status-bar");
    });
  });
});
