import React from "react";
import "./StatusBar.css";

const StatusBar = props => {
  return <div className="status-bar">{props.children}</div>;
};

export default StatusBar;
