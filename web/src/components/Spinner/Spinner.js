import React from "react";
import PropTypes from "prop-types";
import "./Spinner.css";

const Spinner = props => {
  return (
    <div className="spinner" style={props.style}>
      <div className="dot1" />
      <div className="dot2" />
    </div>
  );
};

Spinner.propTypes = {
  style: PropTypes.object.isRequired
};

export default Spinner;
