import React from "react";
import { createRenderer } from "react-test-renderer/shallow";
import Spinner from "./Spinner";

const setup = propOverrides => {
  const props = Object.assign(
    {
      style: {}
    },
    propOverrides
  );

  const renderer = createRenderer();
  renderer.render(<Spinner {...props} />);
  const output = renderer.getRenderOutput();

  return {
    props: props,
    output: output
  };
};

describe("component", () => {
  describe("Spinner", () => {
    it("should render propertly", () => {
      const { output } = setup();
      expect(output.type).toBe("div");
      expect(output.props.className).toBe("spinner");

      const [div1, div2] = output.props.children;

      expect(div1.type).toBe("div");
      expect(div1.props.className).toBe("dot1");
      expect(div2.type).toBe("div");
      expect(div2.props.className).toBe("dot2");
    });
  });
});
