import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "./actions";
import PropTypes from "prop-types";

import service from "./utils/tweets-service";
import FormMotion from "./components/FormMotion/FormMotion";
import HeightMotion from "./components/HeightMotion/HeightMotion";
import StatusBar from "./components/StatusBar/StatusBar";
import TweetListBuffer from "./components/TweetListBuffer/TweetListBuffer";
import "./App.css";

class App extends Component {
  state = {
    word: "",
    error: false,
    loading: false
  };

  componentDidMount() {
    this.onScrollToBottom(this.loadMore);

    if (this.props.match.params.word) {
      this.props.actions.updateTextboxValue(this.props.match.params.word);
      this.handleWord(this.props.match.params.word);
    }
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.loadMore);
  }

  componentDidUpdate() {
    const isBufferEmpty = this.props.tweetsBuffer.length === 0;
    const hasTweets = this.props.tweets.length > 0;
    if (isBufferEmpty && hasTweets) {
      const tweetsIds = this.props.tweets.map(x => x.id);
      const lastTweetId = tweetsIds.sort((a, b) => a - b)[0];
      const word = this.state.word;

      service
        .getMore(word, lastTweetId)
        .then(this.load)
        .catch(this.loadError);
    }
  }

  handleLoad = word => {
    this.props.actions.navigateTo(`/${word}`);
    this.handleWord(word);
  };

  handleWord = word => {
    this.props.actions.cleanTweetList();
    this.setState({
      word: word,
      error: false,
      loading: true
    });
    service
      .getInfo(word)
      .then(this.load)
      .catch(this.loadError);
  };

  load = info => {
    const firstTen = info.tweets.splice(0, 10);
    this.props.actions.addTweetsToBuffer(info.tweets);
    this.props.actions.addTweetsToList(firstTen);

    this.setState({
      word: info.word,
      loading: false
    });
  };

  loadError = info => {
    this.props.actions.cleanTweetList();
    this.setState({
      loading: false,
      error: true
    });
  };

  onScrollToBottom = callback => {
    window.addEventListener("scroll", callback);
  };

  loadMore = () => {
    var doc = document.documentElement;
    var top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
    var bottom = doc.clientHeight + top;
    var remaining = doc.scrollHeight - bottom;
    var isTweetsBufferEmpty = this.props.tweetsBuffer.length === 0;

    if (remaining < 300 && !isTweetsBufferEmpty) {
      var nextTweet = this.props.tweetsBuffer.slice(0, 1);
      this.props.actions.addTweetsToList(nextTweet);
      this.props.actions.decrementTweetsBuffer();
    }
  };

  onTweetSelected = tweet => {
    const currentWord = this.props.match.params.word;
    this.props.actions.navigateTo(`/${currentWord}/${tweet.id}`, {
      tweet: tweet
    });
  };

  render() {
    const state = this.state;
    const props = this.props;
    return (
      <div className="container">
        <HeightMotion show={!state.word} height={40} className="landing">
          Enter any word and I going to search it on Twitter!
        </HeightMotion>
        <FormMotion loading={state.loading} onChange={this.handleLoad} />
        <TweetListBuffer
          tweets={props.tweets}
          tweetSelected={this.onTweetSelected}
        />
        <HeightMotion show={state.loading} height={30}>
          <StatusBar>
            Looking up tweets with the word "{state.word}"...
          </StatusBar>
        </HeightMotion>
        <HeightMotion show={state.error} height={40} className="landing">
          Sorry, something went wrong
        </HeightMotion>
      </div>
    );
  }
}

App.propTypes = {
  tweets: PropTypes.array.isRequired,
  tweetsBuffer: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  tweets: state.tweets,
  tweetsBuffer: state.tweetsBuffer
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
