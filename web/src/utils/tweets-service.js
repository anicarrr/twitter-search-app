function fetchData(url) {
  const base = window.location.origin;

  const timeout = new Promise((resolve, reject) => {
    setTimeout(reject, 20000, "request timed out");
  });

  const fetchWrapper = new Promise((resolve, reject) => {
    fetch(`${base}/${url}`)
      .then(response => resolve(response))
      .catch(err => reject(err));
  });

  return Promise.race([timeout, fetchWrapper])
    .then(response => (response && response.ok ? response.json() : null))
    .catch(err => {
      console.log("Error fetching data: " + err);
      return null;
    });
}

function getInfo(word) {
  const url = `api/search/${word}`;
  return fetchData(url);
}

function getById(word, tweetId) {
  const url = `api/search/${word}/${tweetId}`;
  return fetchData(url);
}

function getMore(word, tweetId) {
  const url = `api/more/${word}/${tweetId}`;
  return fetchData(url);
}

export default {
  getInfo,
  getById,
  getMore
};
