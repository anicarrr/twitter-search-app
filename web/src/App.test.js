import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore } from "redux";
import App from "./App";

const reducersMock = () => {
  return {
    tweets: [],
    tweetsBuffer: []
  };
};

const store = createStore(reducersMock);

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <Provider store={store}>
      <Router>
        <App match={{ params: "" }} />
      </Router>
    </Provider>,
    div
  );
});
