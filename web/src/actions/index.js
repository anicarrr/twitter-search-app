import { push } from "react-router-redux";

export const addTweetsToList = tweets => ({
  type: "ADD_TWEETS_TO_LIST",
  tweets
});
export const addTweetsToBuffer = tweets => ({
  type: "ADD_TWEETS_TO_BUFFER",
  tweets
});
export const cleanTweetList = () => ({ type: "CLEAN_TWEET_LIST" });
export const decrementTweetsBuffer = () => ({
  type: "DECREMENT_TWEETS_BUFFER"
});
export const updateTextboxValue = value => ({
  type: "UPDATE_TEXTBOX_VALUE",
  value
});
export const navigateTo = (location, params) => push(location, params);
