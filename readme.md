# Twitter Search App

> Web application showing tweets based on a given word.

## Built With

- React
- Redux
- Express
- Mongo

## Getting started

> Prerequisites: you'll need node and npm installed

If you don't have `yarn` installed, install it:
```
$ npm i -g yarn
```

After cloning the repo, install the dependencies. Inside twitter-search-app folder, run:
```
$ yarn install
```

And then run the front-end and back-end with one command:
```
$ yarn start
```

By default this run the front-end at http://localhost:3000/ and the back-end at http://localhost:8080/. Both are watching for changes, so there is no need to re-start it after changes.  


In order to use the twitter api you need an [API key and API secret](https://dev.twitter.com/apps).  
Once you have them, create a `.env` file inside the `server` folder, like this:
```
TWITTER_CONSUMER_KEY=yourkey
TWITTER_CONSUMER_SECRET=yoursecret
```

> In this case I already provided the .env file with my own api key and api secret key

## Database

If you don't specify a `MONGO_URL` in your .env file, this web app will run with a mocked db. This means that every request will go to the Twitter API without using any cached information from the user.  
To use mongo you have to use your own instance locally.  

> Don't forget to add (or uncomment) MONGO_URL=mongodb://localhost:27017/twitterSearchApp in the .env file.

### Cache 

There are 2 variables in the .env file that you can use to manage cache.

```
CACHE=true
CACHE_MINUTES=1 
```

CACHE let you turn off/on the usage of cache.
CACHE_MINUTES let you set a range of time (in minutes) where your data is going to live.

## Production

Just run the following command at the root folder:

```
$ yarn build
```

The folder 'build' will be created with all the data ready to be deployed.

# How it works

Enter any word on the input box and press ENTER or click on the 'Load' text button. A list will be shown with the first 10 tweets found, if you scroll down more tweets will be add to the list, you can enter on any of them by clicking.

Under the hood it works the following way:
When a given word is provided it will fetch the first 100 tweets from Twitter and it going to be persist in mongo and it will return the tweets to the user. The first 10 tweets are shown while the 90 remaining will be store in a buffer state in the app, every time the user scroll down tweets will be transfer from the buffer state to the current list, when the buffer gets empty it going to make another request to get another 100 and process repeat again. 

If the user has the cache active and re-enter the same word to search and the cache time has not expire yet, mongo will return the data with the same process as mentioned before, when all the data has been sent the app will request to the Twitter API again and will continue persisting in mongo with the other stored tweets and repeating the well-known proccess.  


